const express = require("express");
const bodyParser = require("body-parser");
const allData = require("./routes/allData.js");
const cityDetails = require("./routes/cityDetails.js");
const futureTemps = require("./routes/futureTemps.js");
const app = express();
const port = 8000;
app.use(bodyParser.json());
app.use("/cityDetails", cityDetails);
app.use("/all-timezone-cities", allData);
app.use("/futureTemps", futureTemps);

// app.get("/all-timezone-cities", (req, res) => {
//   res.send("hello from all");
// });
app.listen(port, () => {
  console.log("server running on port 8000");
});
