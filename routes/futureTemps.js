const express = require("express");
const router = express.Router();
const { fork } = require("child_process");

let tempdata = {
  temperature: [],
};
router.post("/", (req, res) => {
  // let name = req.query.city;
  // let hours = req.body.hours;
  // for (let i = 1; i <= hours; i++) {
  //   tempdata.temperature.push("25°C");
  // }
  // let datajson = JSON.stringify(tempdata);
  // tempdata.temperature = [];
  // res.send(datajson);
  const child = fork(__dirname + "/post1.js");

  child.on("message", (message) => {
    console.log("Returning /total results");
    // res.setHeader("Content-Type", "application/json");
    // res.writeHead(200);
    res.send(message);
  });

  child.send(req.body.hours);
});
module.exports = router;
